FROM openjdk:8-jre-alpine

LABEL maintainer="Rik de Groot <rik@easee.online>"
LABEL version="1.0"
LABEL description="testingbot tunnel image based on testingbot-tunnel.jar from https://testingbot.com/support/other/tunnel"

ENV TESTINGBOT_API_KEY "key"
ENV TESTINGBOT_API_SECRET "secret"

RUN apk add --update --virtual=.dependencies \
      wget \
      unzip && \
\
    apk add bash && \
\
    mkdir -p /var/opt && \
    cd /var/opt && \
\
    wget -q https://testingbot.com/downloads/testingbot-tunnel.zip && \
\
    unzip -qq testingbot-tunnel.zip && \
\
    rm -f /var/opt/testingbot-tunnel.zip && \
    apk del .dependencies && \
    rm -rf /tmp/* /var/cache/apk/*

COPY entrypoint.sh /usr/local/bin/entrypoint
COPY healthcheck.sh /usr/local/bin/healthcheck

WORKDIR /var/opt/testingbot-tunnel

EXPOSE 4445

ENTRYPOINT ["entrypoint"]

HEALTHCHECK  CMD healthcheck
