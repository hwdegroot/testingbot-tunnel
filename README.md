## Description

If you want to use the testingbot tunnel in a firewalled environment (ie gitlab-ci) use this image to proxy selenium traffic.
Read more on the [testingbot tunnel](https://testingbot.com/support/other/tunnel) documentation page

## Usage

    docker run -t registry.gitlab.com/easeeonline/docker/testingbot-tunnel:latest <key> <secret>

or

    docker run -t -e TESTINGBOT_API_KEY=<key> -e TESTINGBOT_API_SECRET=<secret> registry.gitlab.com/easeeonline/docker/testingbot-tunnel:latest
