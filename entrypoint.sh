#!/bin/bash

# Remove the PID file, so that the tunnel gets closed before shutting down
# https://testingbot.com/support/other/tunnel#shutting-down-tunnel
# http://blog.milesmaddox.com/2016/10/31/Docker_Shutdown_Hooks/

function gracefulshutdown {
    echo "Killing testingbot..."
    rm -f /var/opt/testingbot-tunnel/testingbot-tunnel.pid
    echo "Shutting down!"
}

trap gracefulshutdown SIGTERM
trap gracefulshutdown SIGINT

rm -f /tmp/health /var/log/testingbot-tunnel.log
mkdir -p /var/log

if [[ $# -eq 1 ]]; then
    echo 1 > /tmp/health
    eval $@
else
    echo 0 > /tmp/health
    java -jar /var/opt/testingbot-tunnel/testingbot-tunnel.jar ${1:-$TESTINGBOT_API_KEY} ${2:-$TESTINGBOT_API_SECRET} 2>&1 | tee /var/log/testingbot-tunnel.log
fi


