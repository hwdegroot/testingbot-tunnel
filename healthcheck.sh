#!/bin/bash

if [[ ! -f /tmp/health ]] || [[ ! -f /var/log/testingbot-tunnel.log ]]; then
    exit 1
fi

grep "The Tunnel is ready, ip:" /var/log/testingbot-tunnel.log | wc -l > /tmp/health

status=`cat /tmp/health`

if [[ $((status)) -eq 0 ]]; then
    exit 1
fi

exit 0

